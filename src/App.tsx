
import { Route, Routes, HashRouter } from "react-router-dom";

import CheckoutPage from "./component/CheckoutPage";
import "./App.css";


function App() {

  return (
    <HashRouter>
      <Routes>
        <Route
          path="/"
          element={
              <CheckoutPage />
          }
        />
      </Routes>
    </HashRouter>
  );
}

export default App;
