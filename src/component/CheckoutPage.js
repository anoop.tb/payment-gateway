import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Container, Box, FormControl, FormLabel } from "@mui/material";
import applePay from "../Assets/applePay.png";
import "../App.css";
import ArrowCircleRightOutlinedIcon from "@mui/icons-material/ArrowCircleRightOutlined";

const CheckoutPage = () => {
  const applepayHandler = () => {
    console.log("Apple Pay");
    if (!window.ApplePaySession) {
      return;
    }
    const request = {
      countryCode: "US",
      currencyCode: "USD",
      merchantCapabilities: ["supports3DS"],
      supportedNetworks: ["visa", "masterCard", "amex", "discover"],
      total: {
        label: "Demo (Card is not charged)",
        type: "final",
        amount: "1.99",
      },
    };
    // Create ApplePaySession
    console.log("Create Apple Session");
    const session = new window.ApplePaySession(3, request);
    console.log("After Apple Session Creation");

    session.onvalidatemerchant = async (event) => {
      console.log("On Validate Merchat");
      // Call your own server to request a new merchant session.
      //  const merchantSession = await validateMerchant();
      //  session.completeMerchantValidation(merchantSession);
      try {
        const data = {
          method: "post",
          body: {
            merchantIdentifier: "com.manbat.dev",
            displayName: "mystore",
            initiative: "web",
            initiativeContext: window.location.hostname,
          },
          header: { "Content-Type": "application/json" },
        };
        const merchantSession = await fetch(
          "https://apple-pay-gateway.apple.com/paymentservices/paymentSession",
          data
        );
        console.log(merchantSession);
        // this.$axios.$post('/apple_pay_session', data);
        if (merchantSession && merchantSession.merchantSessionIdentifier) {
          session.completeMerchantValidation(merchantSession);
          console.log(merchantSession);
        } else {
          session.abort();
        }
      } catch (error) {
        session.abort();
      }
    };

    session.onpaymentmethodselected = (event) => {
      // Define ApplePayPaymentMethodUpdate based on the selected payment method.
      // No updates or errors are needed, pass an empty object.
      const update = {};
      session.completePaymentMethodSelection(update);
    };
    session.onpaymentauthorized = (event) => {
      // Define ApplePayPaymentAuthorizationResult
      const result = {
        status: window.ApplePaySession.STATUS_SUCCESS,
      };
      session.completePayment(result);
    };
    session.oncancel = (event) => {
      // Payment canceled by WebKit
      // console.log("user cancelled the payment")
    };

    session.begin();
  };

  return (
    <Container sx={{ display: "flex", justifyContent: "center" }}>
      <Box
        display="flex"
        alignItems="center"
        height="50vh"
        sx={{
          marginTop: "50px",
          border: "5px solid #ccc",
          boxShadow: "0 2px 4px rgba(0,0,0,0,1)",
          padding: 2,
        }}
      >
        <FormControl component="fieldset">
          <FormLabel sx={{ textAlign: "center", fontSize: "22px" }}>
            Pay : $1.99
          </FormLabel>
          <form
            action="https://sbcheckout.payfort.com/FortAPI/paymentPage"
            method="post"
          >
            <input type="hidden" name="service_command" value="TOKENIZATION" />
            <input type="hidden" name="language" value="en" />
            <input type="hidden" name="merchant_identifier" value="9b3641cf" />
            <input
              type="hidden"
              name="access_code"
              value="lJJTF60VpaGLL9ZdTdB7"
            />
            <input
              type="hidden"
              name="signature"
              value="dcb6f0784c825546aef1b57ce9a2b10a3741d360f5d54e1bbf46196bedcd5c2d"
            />
            <input
              type="hidden"
              name="return_url"
               value="http://accioninternalproject.s3-website-us-west-2.amazonaws.com/MerchantPage"
              //               value="http://localhost:3000/MerchantPage"

            />
            <input
              type="hidden"
              name="merchant_reference"
              value="3509167138PDG"
            />
            <input
          type="hidden"
          name="amount"
          value="10000"
        />
        <input
          type="hidden"
          name="currency"
          value="AED"
        />
            <Button
              sx={{ marginTop: "10px" }}
              color="primary"
              variant="contained"
              type="submit"
              id="form1"
            >
              {" "}
              Proceed to card Payment <ArrowCircleRightOutlinedIcon />
            </Button>
          </form>
          {window.ApplePaySession && (
            <Button
              onClick={applepayHandler}
              sx={{ marginTop: "10px" }}
              variant="outlined"
              color="primary"
            >
              <img src={applePay} />
            </Button>
          )}
        </FormControl>
      </Box>
    </Container>
  );
};
export default CheckoutPage;